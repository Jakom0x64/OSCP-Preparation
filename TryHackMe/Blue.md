# Blue

![alt logo](https://i.imgur.com/gwscVjv.gif)
- Name: Blue
- Descrition: Deploy & hack into a Windows machine, leveraging common misconfigurations issues.

## Recon:

- How many ports are open with a port number under 1000?
  ```bash
  nmap -p0-1000 -sC [MACHINE_IP] -oN nmap_scan && cat nmap_scan | grep open | wc -l
  ```
  - Answer: 3.
- What is this machine vulnerable to? (Answer in the form of: ms??-???, ex: ms08-067)
  ```bash
  nmap -Pn -p445 --script smb-vuln* [MACHINE_IP]
  ```
  - Answer: ms17-010.

## Gain Access:

- Find the exploitation code we will run against the machine. What is the full path of the code? (Ex: exploit/........)
  - Answer: exploit/windows/smb/ms17_010_eternalblue.
- Show options and set the one required value. What is the name of this value? (All caps for submission)
  - Answer: rhosts.

## Escalate:

- If you haven't already, background the previously gained shell (CTRL + Z). Research online how to convert a shell to meterpreter shell in metasploit. What is the name of the post module we will use? (Exact path, similar to the exploit we previously selected)
  - Answer: post/multi/manage/shell_to_meterpreter.
- Select this (use MODULE_PATH). Show options, what option are we required to change?
  - Answer: session.

## Cracking:

- Within our elevated meterpreter shell, run the command 'hashdump'. This will dump all of the passwords on the machine as long as we have the correct privileges to do so. What is the name of the non-default user?
  - Answer: Jon.
- Copy this password hash to a file and research how to crack it. What is the cracked password?
  - Answer: alqfna22. [CrackStation](https://crackstation.net/) *you can crack it with john tool, but i prefer the easy way*

## Find flags!

- Flag1? This flag can be found at the system root.
  ```
  type C:\flag1.txt
  ```
  - Answer: *content of flag1.txt*.
- Flag2? This flag can be found at the location where passwords are stored within Windows.
  ```
  type C:\Windows\System32\config\flag2.txt
  ```
  - Answer: *content of flag2.txt*.
- flag3? This flag can be found in an excellent location to loot. After all, Administrators usually have pretty interesting things saved.
  ```
  type C:\Users\Jon\Documents\flag3.txt
  ```
  - Answer: *content of flag3.txt*.

